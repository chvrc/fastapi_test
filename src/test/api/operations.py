from fastapi import APIRouter


router = APIRouter(
    prefix='/operations',
)


@router.get('/')
async def get_operations():
    return []
from fastapi import APIRouter

from typing import Optional


router = APIRouter(
    prefix='/bot',
)


@router.get('/{bot_id}')
async def get_bot_id(bot_id: int, q: Optional[str]):
    return {"bot_id": bot_id, 'q': q}
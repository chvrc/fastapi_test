from fastapi import APIRouter

from .operations import router as operations_router
from .bot import router as bot_router


router = APIRouter()
router.include_router(operations_router)
router.include_router(bot_router)